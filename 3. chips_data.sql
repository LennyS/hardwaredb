INSERT INTO chips (ChipType, ManufacturerID, ChipID)
VALUES ('GTX 1050', 1, 1), ('GTX 1050TI', 1, 2), ('GTX 1060', 1, 3), ('GTX 1070', 1, 4),
       ('GTX 1070TI', 1, 5), ('GTX 1080', 1, 6) , ('GTX 1080TI', 1, 7) , ('RX 550', 2, 8),
       ('RX 560', 2, 9), ('RX 570', 2, 10), ('RX 580', 2, 11), ('VEGA 56', 2, 12), ('VEGA 64', 2, 13);
