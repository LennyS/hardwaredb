INSERT INTO parts (partsID, partsName, ManufacturerID, ChipID)
VALUES (1, 'EVGA GTX 1080', 1, 6),
(2, 'ASUS GTX 1050', 2, 1),
(3, 'MSI 1050TI', 4, 2),
(4, 'MSI GTX 1060', 4, 3),
(5, 'ASUS GTX 1070', 2, 4),
(6, 'EVGA GTX 1070TI', 1, 5),
(7, 'EVGA GTX 1080TI', 1, 7) ,
(8, 'GIGABYTE RX 550', 3, 8),
(9, 'SAPPHIRE RX 560', 6, 9),
(10, 'SAPPHIRE RX 570', 6, 10),
(11, 'XFX RX 580', 5, 11),
(12, 'XFX VEGA 56', 5, 12),
(13, 'GIGABYTE VEGA 64', 3, 13);