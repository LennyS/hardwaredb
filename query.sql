
SELECT PartsName FROM parts WHERE partsID = 1;

SELECT ManufacturerID FROM parts WHERE partsID = 2;

SELECT StoreName FROM stores WHERE storeID = 1;

SELECT storeID FROM part_store_mapper WHERE partsID = 1;


-- -----------------------------------------------------------

SELECT ChipType, m.ManufacturerID, m.ManufacturerName FROM chips
INNER JOIN manufacturers m ON chips.ManufacturerID = m.ManufacturerID;

SELECT partsName, m.ManufacturerID, m.ManufacturerName FROM parts
INNER JOIN manufacturer2 m ON parts.ManufacturerID = m.ManufacturerID;

SELECT s.storeName, partsID FROM part_store_mapper
INNER JOIN stores s on part_store_mapper.storeID = s.storeID WHERE partsID = 1;

SELECT storeID, p.partsName FROM part_store_mapper
INNER JOIN parts p on part_store_mapper.partsID = p.partsID WHERE storeID = 1;

-- -----------------------------------------------------------


SELECT part_store_mapper.partsID, part_store_mapper.storeID, p.partsName, m.ManufacturerName
FROM ((part_store_mapper
    INNER JOIN parts p on part_store_mapper.partsID = p.partsID)
    INNER JOIN manufacturers m on p.ManufacturerID = m.ManufacturerID));

SELECT part_store_mapper.storeID, part_store_mapper.partsID, s.storeName, p.partsName
FROM ((part_store_mapper
    INNER JOIN parts p on part_store_mapper.partsID = p.partsID)
    INNER JOIN stores s on part_store_mapper.storeID = s.storeID);

