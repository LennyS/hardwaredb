DROP DATABASE hardwaredb;
CREATE DATABASE hardwaredb;

USE hardwaredb;

CREATE TABLE manufacturers (
  ManufacturerID   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ManufacturerName VARCHAR(50)     NOT NULL
);

CREATE TABLE manufacturer2 (
  ManufacturerID   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ManufacturerName VARCHAR(50)     NOT NULL
);

CREATE TABLE chips (
  ChipID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  ChipType VARCHAR(50) NOT NULL,

  ManufacturerID INT NOT NULL,

  FOREIGN KEY (ManufacturerID) REFERENCES manufacturers (ManufacturerID)
);

CREATE TABLE parts (
  partsID   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  partsName VARCHAR(50)     NOT NULL,

  ManufacturerID INT NOT NULL,
  ChipID INT NOT NULL,

  FOREIGN KEY (ManufacturerID) REFERENCES manufacturer2 (ManufacturerID),

  FOREIGN KEY (ChipID) REFERENCES chips (ChipID)
);

CREATE TABLE stores (
  storeID   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  storeName VARCHAR(50)     NOT NULL
);

CREATE TABLE part_store_mapper (
  MapID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,

  partsID INT NOT NULL,
  storeID INT NOT NULL,

  FOREIGN KEY (partsID) REFERENCES parts (partsID),
  FOREIGN KEY (storeID) REFERENCES stores (storeID)
);